/*
    <one line to give the library's name and an idea of what it does.>
    Copyright (C) 2011  Martin Klapetek <martin.klapetek@gmail.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/


#include "salut-message-widget.h"

#include "common/circular-countdown.h"

#include <KLocalizedString>
#include <KIcon>

#include <QAction>
#include <QLayout>
#include <QWidgetAction>
#include <QVBoxLayout>

SalutMessageWidget::SalutMessageWidget(QWidget *parent)
    : KMessageWidget(parent)
{
    setMessageType(KMessageWidget::Information);
    setWordWrap(true);
    QSize sz = size();
    sz.setWidth(parent->size().width());
    resize(sz);

    setCloseButtonVisible(false);

    CircularCountdown *circCountdown = new CircularCountdown(8000, this);

    connect(circCountdown, SIGNAL(timeoutReached()), this, SIGNAL(okayPressed()));

    QAction *okayAction = new QAction(KIcon("dialog-ok-apply"), i18n("Okay"), this);
    connect(okayAction, SIGNAL(triggered(bool)), this, SIGNAL(okayPressed()));
    addAction(okayAction);

    QAction *noAction = new QAction(KIcon("process-stop"), i18n("No, let me change it"), this);
    connect(noAction, SIGNAL(triggered(bool)), this, SIGNAL(noPressed()));
    addAction(noAction);

    QAction *cancelAction = new QAction(KIcon("process-stop"), i18n("Cancel"), this);
    connect(cancelAction, SIGNAL(triggered(bool)), this, SIGNAL(cancelPressed()));
    addAction(cancelAction);

    circCountdown->move(this->width()-22, 6);
    circCountdown->start();
}

SalutMessageWidget::~SalutMessageWidget()
{
}

///these params always comes from KUser with first & last name splitted manualy by the last space
void SalutMessageWidget::setParams(const QString& firstname, const QString& lastname, const QString& nick)
{
    //either one of the names is filled and nick is filled
    if ((lastname.isEmpty() && !firstname.isEmpty()) || (!lastname.isEmpty() && firstname.isEmpty())
            && !nick.isEmpty()) {

        setText(i18n("Your local network account was set with name %1 and nickname %2, okay?",
                     lastname.isEmpty() ? firstname : lastname, nick));

    //either one of the names is filled and nick is empty
    } else if ((lastname.isEmpty() && !firstname.isEmpty()) || (!lastname.isEmpty() && firstname.isEmpty())
            && nick.isEmpty()) {

        setText(i18n("Your local network account was set with name %1, okay?",
                     lastname.isEmpty() ? firstname : lastname));

    //both firs & last names are empty but nick is not
    } else if (lastname.isEmpty() && firstname.isEmpty() && !nick.isEmpty()) {
        setText(i18n("Your local network account was set with nick %1, okay?",
                     nick));

    } else if (lastname.isEmpty() && firstname.isEmpty() && nick.isEmpty()) {
        //FIXME: let the user know that he reached a very strange situation

    } else {
        setText(i18n("Your local network account was set with first name %1, lastname %2 and nickname %3, okay?",
                     firstname, lastname, nick));
    }
}
