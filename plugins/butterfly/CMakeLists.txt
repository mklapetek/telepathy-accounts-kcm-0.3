project (kcmtelepathyaccounts-plugin-butterfly)

include_directories (${CMAKE_CURRENT_BINARY_DIR}
                     ${CMAKE_CURRENT_SOURCE_DIR}
)

set (kcmtelepathyaccounts_plugin_butterfly_SRCS
     butterfly-account-ui-plugin.cpp
     butterfly-account-ui.cpp
     main-options-widget.cpp
)

kde4_add_ui_files (kcmtelepathyaccounts_plugin_butterfly_SRCS
                   main-options-widget.ui
)

kde4_add_plugin (kcmtelepathyaccounts_plugin_butterfly
                 ${kcmtelepathyaccounts_plugin_butterfly_SRCS}
)

target_link_libraries (kcmtelepathyaccounts_plugin_butterfly
                       kcmtelepathyaccounts
                       ${QT_LIBRARIES}
                       ${KDE4_KDEUI_LIBS}
                       ${TELEPATHY_QT4_LIBRARIES}
)

# Install:
install (TARGETS kcmtelepathyaccounts_plugin_butterfly
         DESTINATION ${PLUGIN_INSTALL_DIR}
)

install (FILES kcmtelepathyaccounts_plugin_butterfly.desktop
         DESTINATION ${SERVICES_INSTALL_DIR}
)

